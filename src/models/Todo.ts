export interface ITodo {
  id: number;
  title: string;
  userId: number;
  completed: boolean;
}

export interface INewTodo {
  title: string;
  userId: number;
  completed: boolean;
}
