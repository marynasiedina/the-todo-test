import { combineReducers, configureStore } from '@reduxjs/toolkit';
import todoReducer from './reducers/todoSlice'

const rootReducer = combineReducers({
  todoReducer
  // add other reducers here
})

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
  })
}

export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']
