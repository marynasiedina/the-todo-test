import { FC, useEffect } from 'react'
import { useAppSelector, useAppDispatch } from '../hooks/redux'
import { fetchAllTodos, removeTodo, updateTodo } from '../store/reducers/todoActionCreators'
import { ITodo } from '../models/Todo'
import { List, CircularProgress, Box } from '@mui/material';

import TodoItem from './TodoItem';

const TodoList: FC = () => {
  const { todos, isLoading, error } = useAppSelector(state => state.todoReducer)
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(fetchAllTodos());
  }, [dispatch])

  const handleRemoveTodo = (todo: ITodo) => {
    dispatch(removeTodo(todo))
  }
  const handleToggleTodo = (todo: ITodo) => {
    dispatch(updateTodo({ ...todo, completed: !todo.completed }))
  }

  return (
    <List>
      {error && <h1>{error}</h1>}
      {todos.map((todo, i) =>
        <TodoItem key={i} todo={todo} remove={handleRemoveTodo} update={handleToggleTodo} />
      )}
      {isLoading && <Box sx={{ display: 'flex' }}>
        <CircularProgress />
      </Box>}
    </List>
  );
}

export default TodoList;
