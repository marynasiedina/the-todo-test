import TodoList from './components/TodoList'
import AddTodo from './components/AddTodo'
import { Container, AppBar } from '@mui/material';

const App = () => {
  return (
    <>
      <AppBar style={{ height: '60px', textAlign: 'center', padding: '20px 0' }}>My TODOS</AppBar>
      <Container style={{ marginTop: '60px' }}>
        <AddTodo />
        <TodoList />
      </Container>
    </>
  );
}

export default App;
