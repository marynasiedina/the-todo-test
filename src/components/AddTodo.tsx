import React, { FC, useState } from 'react';
import { useAppDispatch } from '../hooks/redux'
import { createTodo } from '../store/reducers/todoActionCreators'
import { Box, TextField, Button } from '@mui/material';

const AddTodo: FC = () => {
  const [newTodo, setNewTodo] = useState('');
  const dispatch = useAppDispatch()

  const handleCreateTodo = () => {
    dispatch(createTodo({ title: newTodo, userId: 1, completed: false }))
    setNewTodo('')
  }
  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewTodo(e.target.value)
  }

  return (
    <Box style={{ margin: '5px', paddingTop: '20px', display: 'flex', alignItems: 'center' }}>
      <TextField id='standard-basic' variant='standard' type='text' label='Type Todo' value={newTodo} onChange={handleOnChange} />
      <Button disabled={!newTodo} variant="contained" type='button' onClick={handleCreateTodo}>Add todo</Button>
    </Box>
  );
}

export default AddTodo;
