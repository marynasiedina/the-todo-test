import React, { FC } from 'react';
import { ListItem, IconButton } from '@mui/material';
import { ITodo } from '../models/Todo';
import DeleteIcon from '@mui/icons-material/Delete';

interface TodoItemProps {
  todo: ITodo;
  remove: (todo: ITodo) => void;
  update: (todo: ITodo) => void;
}

const TodoItem: FC<TodoItemProps> = ({ todo, remove, update }) => {

  const handleRemoveTodo = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, todo: ITodo) => {
    e.stopPropagation()
    remove(todo)
  }
  return (
    <ListItem
      onClick={() => update(todo)}
      style={{ textDecorationLine: todo.completed ? 'line-through' : 'none', border: '1px solid grey', margin: '5px' }}
      secondaryAction={
        <IconButton edge="end" aria-label="delete" onClick={(e) => handleRemoveTodo(e, todo)} >
          <DeleteIcon />
        </IconButton>
      }
    >
      {todo.title}
    </ListItem>
  );
}

export default TodoItem;
