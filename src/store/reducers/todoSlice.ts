import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createTodo, updateTodo, removeTodo, fetchAllTodos } from './todoActionCreators';
import { ITodo } from '../../models/Todo';

interface TodoState {
  todos: ITodo[];
  isLoading: boolean;
  error: string;
}

const initialState: TodoState = {
  todos: [],
  isLoading: false,
  error: '',
}

export const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {},
  extraReducers: {
    // fetchAllTodos
    [fetchAllTodos.fulfilled.type]: (state, action: PayloadAction<ITodo[]>) => {
      state.isLoading = false;
      state.todos = action.payload;
      state.error = '';
    },
    [fetchAllTodos.pending.type]: (state) => {
      state.isLoading = true;
    },
    [fetchAllTodos.rejected.type]: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    // createTodo
    [createTodo.fulfilled.type]: (state, action: PayloadAction<ITodo>) => {
      state.isLoading = false;
      state.error = '';
      state.todos.push(action.payload)
    },
    [createTodo.pending.type]: (state) => {
      state.isLoading = true;
    },
    [createTodo.rejected.type]: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    // updateTodo
    [updateTodo.fulfilled.type]: (state, action: PayloadAction<ITodo>) => {
      state.error = '';
      state.todos = state.todos.map(todo => todo.id === action.payload.id ? { ...action.payload } : todo)
    },
    [updateTodo.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
    },
    // removeTodo
    [removeTodo.fulfilled.type]: (state, action: PayloadAction<ITodo>) => {
      state.error = '';
      state.todos = state.todos.filter(todo => todo.id !== action.payload.id)
    },
    [removeTodo.rejected.type]: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
    },
  }
})

export default todoSlice.reducer;
