import axios from 'axios';
import { ITodo, INewTodo } from '../../models/Todo';
import { createAsyncThunk } from '@reduxjs/toolkit';

// "createAsyncThunk" itself handle dispatching lifecycle methods for our request: pending, fulfilled, and rejected,
//  which we handle within our slice

const baseURL = 'https://jsonplaceholder.typicode.com/todos'

export const fetchAllTodos = createAsyncThunk(
  'todo/fetchAll',
  async (_, thunkAPI) => {
    try {
      const response = await axios.get<ITodo[]>('https://jsonplaceholder.typicode.com/user/1/todos?_limit=10')
      return response.data;
    } catch (e) {
      return thunkAPI.rejectWithValue("Bad request")
    }
  }
)

export const createTodo = createAsyncThunk(
  'todo/createTodo',
  async (data: INewTodo, thunkAPI) => {
    try {
      const response = await axios.post<INewTodo>(baseURL, data)
      return response.data;
    } catch (e) {
      return thunkAPI.rejectWithValue("Todo has not created")
    }
  }
)

export const updateTodo = createAsyncThunk(
  'todo/updateTodo',
  async (data: ITodo, thunkAPI) => {
    try {
      const response = await axios.put<ITodo>(`${baseURL}/${data.id}`, data)
      return response.data;
    } catch (e) {
      return thunkAPI.rejectWithValue("Todo has not updated")
    }
  }
)

export const removeTodo = createAsyncThunk(
  'todo/removeTodo',
  async (data: ITodo, thunkAPI) => {
    try {
      const response = await axios.delete(`${baseURL}/${data.id}`)
      return response.status === 200 ? data : null;
    } catch (e) {
      return thunkAPI.rejectWithValue("Todo has not deleted")
    }
  }
)
